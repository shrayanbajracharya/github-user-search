package com.example.android.githubusersearch.data.ui;

public class UserProfileUi {

    private int userId;
    private String username;
    private String name;
    private String avatarUrl;
    private int followers;
    private int following;
    private int publicRepos;

    public UserProfileUi() {
    }

    public UserProfileUi(int userId, String username, String name, int following) {
        this.userId = userId;
        this.username = username;
        this.name = name;
        this.following = following;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public int getPublicRepos() {
        return publicRepos;
    }

    public void setPublicRepos(int publicRepos) {
        this.publicRepos = publicRepos;
    }
}
