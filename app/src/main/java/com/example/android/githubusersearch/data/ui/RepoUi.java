package com.example.android.githubusersearch.data.ui;

public class RepoUi {

    private String repoName;
    private String language;
    private long watchers;

    // Constructors
    public RepoUi() {
    }

    public RepoUi(String repoName, String language, long watchers) {
        this.repoName = repoName;
        this.watchers = watchers;
        this.language = language;
    }

    // Getters and Setters
    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public long getWatchers() {
        return watchers;
    }

    public void setWatchers(long watchers) {
        this.watchers = watchers;
    }
}
