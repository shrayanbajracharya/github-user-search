package com.example.android.githubusersearch.ui.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.android.githubusersearch.R;
import com.example.android.githubusersearch.data.ResponseWrapper;
import com.example.android.githubusersearch.data.ui.UserProfileUi;
import com.example.android.githubusersearch.ui.repo.RepoActivity;

public class ProfileActivity extends AppCompatActivity {

    ImageView avatar;
    TextView userId;
    TextView username;
    TextView name;
    TextView followers;
    TextView following;
    TextView publicRepos;
    Button viewPublicRepos;

    ProgressBar loadingBar;
    TextView emptyState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Mapping views required to be displayed
        avatar = findViewById(R.id.avatar);
        userId = findViewById(R.id.user_id);
        username = findViewById(R.id.username);
        name = findViewById(R.id.name);
        followers = findViewById(R.id.followers);
        following = findViewById(R.id.following);
        publicRepos = findViewById(R.id.public_repos);
        viewPublicRepos = findViewById(R.id.view_public_repos);

        final Intent intent = getIntent();
        if (intent != null) {
            // Retrieving username from search
            final String intentUsername = intent.getStringExtra("username");

            // Displaying username in ActionBar
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(
                        getString(R.string.result_action_bar_title)
                );
            }

            viewPublicRepos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentRepoList = new Intent(
                            ProfileActivity.this, RepoActivity.class);
                    intentRepoList.putExtra("username", intentUsername);
                    startActivity(intentRepoList);
                }
            });

            // Instantiating ViewModel (UserProfile)
            UserProfileViewModel profileViewModel =
                    ViewModelProviders.of(this, new UserProfileViewModelFactory(intentUsername))
                            .get(UserProfileViewModel.class);

            // Observing liveData from ViewModel (UserProfile)
            profileViewModel.getProfileUiLiveData().observe(this,
                    new Observer<ResponseWrapper<UserProfileUi>>() {
                        @Override
                        public void onChanged(
                                ResponseWrapper<UserProfileUi> userProfileUiResponseWrapper) {

                            if (userProfileUiResponseWrapper.isNotSuccessful()) {

                                // Making loading progress bar disappear
                                loadingBar = findViewById(R.id.loadingBar);
                                loadingBar.setVisibility(View.INVISIBLE);

                                // Showing empty state message
                                emptyState = findViewById(R.id.empty_state);
                                emptyState.setVisibility(View.VISIBLE);
                                emptyState.setText(
                                        String.format(
                                                "No user with username %s found",
                                                intentUsername
                                        )
                                );
                                return;
                            }

                            // Displaying data to views using UserProfileUi
                            displayData(
                                    userProfileUiResponseWrapper
                                            .getResponseBody());
                        }
                    });

            // Fetching data for UserProfile (send Request and receive Response)
            profileViewModel.fetchUserProfileFromNetwork();
        }

    }

    public void displayData(UserProfileUi profileUi) {

        // Preparing string
        String stId = "ID: " + profileUi.getUserId();
        String stUsername = "Username: " + profileUi.getUsername();
        String stName = "Name: " + profileUi.getName();
        String stFollowers = "Followers: " + profileUi.getFollowers();
        String stFollowing = "Following: " + profileUi.getFollowing();
        String stPublicRepos = "Public Repos: " + profileUi.getPublicRepos();

        loadingBar = findViewById(R.id.loadingBar);
        loadingBar.setVisibility(View.GONE);

        // Displaying data to views
        Glide.with(this)
                .load(profileUi.getAvatarUrl())
                .into(avatar);
        userId.setText(stId);
        username.setText(stUsername);
        name.setText(stName);
        followers.setText(stFollowers);
        following.setText(stFollowing);
        publicRepos.setText(stPublicRepos);
        viewPublicRepos.setVisibility(View.VISIBLE);
    }

}
