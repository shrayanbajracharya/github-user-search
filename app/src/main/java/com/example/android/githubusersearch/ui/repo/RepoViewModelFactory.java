package com.example.android.githubusersearch.ui.repo;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class RepoViewModelFactory implements ViewModelProvider.Factory {

    private String username;

    RepoViewModelFactory(String username){
        this.username = username;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new RepoViewModel(username);
    }
}
