package com.example.android.githubusersearch.data;

public class ResponseWrapper<T> {

    private boolean isSuccessful;
    private String errorMessage;
    private T responseBody;

    public ResponseWrapper(){
    }

    public ResponseWrapper(boolean isSuccessful, String errorMessage, T responseBody) {
        this.isSuccessful = isSuccessful;
        this.errorMessage = errorMessage;
        this.responseBody = responseBody;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public boolean isNotSuccessful() {
        return !isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }
}
