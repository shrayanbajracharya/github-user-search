package com.example.android.githubusersearch.data.remote.response.profile;

import com.google.gson.annotations.SerializedName;

public class UserProfile {

    @SerializedName("id")
    private int userId;
    @SerializedName("login")
    private String username;
    @SerializedName("name")
    private String name;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("public_repos")
    private int publicRepos;
    @SerializedName("followers")
    private int followers;
    @SerializedName("following")
    private int following;
    @SerializedName("location")
    private String location;

    public UserProfile() {
    }

    public UserProfile(int userId, String username, String name, String avatarUrl,
                       int publicRepos, int followers, int following, String  location) {
        this.userId = userId;
        this.username = username;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.publicRepos = publicRepos;
        this.followers = followers;
        this.following = following;
        this.location = location;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getPublicRepos() {
        return publicRepos;
    }

    public void setPublicRepos(int publicRepos) {
        this.publicRepos = publicRepos;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
