package com.example.android.githubusersearch.ui.search;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.githubusersearch.R;
import com.example.android.githubusersearch.ui.profile.ProfileActivity;

public class MainActivity extends AppCompatActivity {

    EditText etUsername;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Mapping Views
        etUsername = findViewById(R.id.et_username);
        btnSubmit = findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = etUsername.getText().toString().trim();

                // Checking if the username is empty
                if (TextUtils.isEmpty(username)) {
                    etUsername.setError("Username is required");
                    return;
                }

                // Checking Internet Connection
                if (noInternetConnection()) {
                    Toast.makeText(MainActivity.this,
                            "No Internet Connection",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                // Sending username to ProfileActivity
                Intent intent = new Intent(
                        MainActivity.this,
                        ProfileActivity.class);

                intent.putExtra("username", username);
                startActivity(intent);
            }
        });


    }

    private boolean noInternetConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return !(
                cm.getActiveNetworkInfo() != null
                        && cm.getActiveNetworkInfo().isConnected()
        );
    }

}
