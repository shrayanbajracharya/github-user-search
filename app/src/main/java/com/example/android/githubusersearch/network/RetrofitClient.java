package com.example.android.githubusersearch.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Api instance = null;

    public Api getInstance() {
        if (instance != null) {
            return instance;
        }
        String BASE_ADDRESS = "https://api.github.com/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        instance = retrofit.create(Api.class);
        return instance;
    }

}
