package com.example.android.githubusersearch.ui.repo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.githubusersearch.R;
import com.example.android.githubusersearch.data.ResponseWrapper;
import com.example.android.githubusersearch.data.ui.RepoUi;

import java.util.ArrayList;

public class RepoActivity extends AppCompatActivity {

    RecyclerView repoRecyclerView;
    RepoAdapter repoAdapter = new RepoAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        String username = intent.getStringExtra("username");

        repoRecyclerView = findViewById(R.id.repo_recycler_view);
        repoRecyclerView.setHasFixedSize(true);
        repoRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        repoRecyclerView.setAdapter(repoAdapter);

        RepoViewModel repoViewModel =
                ViewModelProviders
                        .of(this, new RepoViewModelFactory(username))
                        .get(RepoViewModel.class);

        repoViewModel
                .getRepoLiveDataWrapper()
                .observe(this,
                        new Observer<ResponseWrapper<ArrayList<RepoUi>>>() {

                            @Override
                            public void onChanged(
                                    ResponseWrapper<ArrayList<RepoUi>>
                                            arrayListResponseWrapper) {

                                if (arrayListResponseWrapper.isNotSuccessful()) {
                                    Toast.makeText(
                                            RepoActivity.this,
                                            "Failure retrieving repo information",
                                            Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                ArrayList<RepoUi> responseBody =
                                        arrayListResponseWrapper.getResponseBody();

                                if (responseBody == null) {
                                    Toast.makeText(
                                            RepoActivity.this,
                                            "Empty repo list retrieved from server",
                                            Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                ArrayList<RepoUi> repoUiArrayList = new ArrayList<>(responseBody);

                                repoAdapter.add(repoUiArrayList);

                            }
                        });

        repoViewModel.fetchRepoListFromNetwork();

    }

}
