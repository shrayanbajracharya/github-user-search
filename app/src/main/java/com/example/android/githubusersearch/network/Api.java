package com.example.android.githubusersearch.network;

import com.example.android.githubusersearch.data.remote.response.profile.UserProfile;
import com.example.android.githubusersearch.data.remote.response.repo.Repo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Api {

    @GET("users/{user}")
    Call<UserProfile> getUserData(@Path("user") String user);

    @GET("users/{user}/repos")
    Call<ArrayList<Repo>> getReposList(@Path("user") String user);

}
