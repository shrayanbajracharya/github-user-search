package com.example.android.githubusersearch.ui.repo;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.githubusersearch.R;
import com.example.android.githubusersearch.data.ui.RepoUi;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder> {

    private ArrayList<RepoUi> repoUiArrayList = new ArrayList<>();

    public RepoAdapter() {
    }

    public RepoAdapter(ArrayList<RepoUi> repoUiArrayList) {
        this.repoUiArrayList = repoUiArrayList;
    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater =
                LayoutInflater
                        .from(parent.getContext());

        View repoListCustomView =
                inflater.inflate(
                        R.layout.repo_list_layout,
                        parent,
                        false);

        return new RepoViewHolder(repoListCustomView);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {

        RepoUi currentRepoUi = new RepoUi();
        currentRepoUi = repoUiArrayList.get(position);

        String stRepoName = "Name: " + currentRepoUi.getRepoName();
        String stLanguage = "Language: " + currentRepoUi.getLanguage();

        holder.repoName.setText(stRepoName);
        holder.language.setText(stLanguage);

    }

    @Override
    public int getItemCount() {
        return repoUiArrayList.size();
    }

    class RepoViewHolder extends RecyclerView.ViewHolder {

        TextView repoName;
        TextView language;

        RepoViewHolder(@NonNull View itemView) {
            super(itemView);

            repoName = itemView.findViewById(R.id.repo_name);
            language = itemView.findViewById(R.id.language);

        }
    }

    public void add(ArrayList<RepoUi> data) {

        if (data == null) {
            return;
        }

        repoUiArrayList.addAll(data);
        notifyItemRangeInserted(repoUiArrayList.size(), data.size());

    }

}
