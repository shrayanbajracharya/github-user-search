package com.example.android.githubusersearch.ui.profile;

import android.util.Log;

import com.example.android.githubusersearch.data.ResponseWrapper;
import com.example.android.githubusersearch.data.remote.response.profile.UserProfile;
import com.example.android.githubusersearch.data.ui.UserProfileUi;
import com.example.android.githubusersearch.network.Api;
import com.example.android.githubusersearch.network.RetrofitClient;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileViewModel extends ViewModel {

    private Call<UserProfile> call;
    private MutableLiveData<ResponseWrapper<UserProfileUi>> profileWrapperLiveData;

    public UserProfileViewModel(String username) {
        init(username);
    }

    public void init(String username) {
        Api instance = new RetrofitClient().getInstance();
        call = instance.getUserData(username);
        profileWrapperLiveData = new MutableLiveData<>();
    }

    public void fetchUserProfileFromNetwork() {
        final ResponseWrapper<UserProfileUi> wrapperProfileUi = new ResponseWrapper<>();
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(@NonNull Call<UserProfile> call,
                                   @NonNull Response<UserProfile> response) {

                wrapperProfileUi.setSuccessful(
                        response.isSuccessful());

                if (wrapperProfileUi.isNotSuccessful()) {
                    wrapperProfileUi.setErrorMessage("Failure while fetching response");
                    profileWrapperLiveData.postValue(wrapperProfileUi);
                    Log.d("ProfileViewModel: ", "response failure");
                    return;
                }

                UserProfile responseBody = response.body();
                if (responseBody == null) {
                    wrapperProfileUi.setErrorMessage("Null retrieved in Response Body");
                    Log.v("ProfileViewModel: ", "No user found");
                    return;
                }

                UserProfileUi profileUi = transformResponseToProfileUi(responseBody);
                wrapperProfileUi.setErrorMessage("Data retrieval successful");
                wrapperProfileUi.setResponseBody(profileUi);
                profileWrapperLiveData.postValue(wrapperProfileUi);

            }

            @Override
            public void onFailure(@NonNull Call<UserProfile> call,
                                  @NonNull Throwable t) {
                wrapperProfileUi.setSuccessful(false);
                wrapperProfileUi.setErrorMessage(t.getLocalizedMessage());
                profileWrapperLiveData.postValue(wrapperProfileUi);
            }
        });
    }

    public UserProfileUi transformResponseToProfileUi(UserProfile responseBody) {
        UserProfileUi result = new UserProfileUi();

        result.setUserId(
                responseBody.getUserId()
        );

        result.setUsername(
                responseBody.getUsername()
        );

        result.setName(
                responseBody.getName()
        );

        result.setAvatarUrl(
                responseBody.getAvatarUrl()
        );

        result.setFollowers(
                responseBody.getFollowers()
        );

        result.setFollowing(
                responseBody.getFollowing()
        );

        result.setPublicRepos(
                responseBody.getPublicRepos()
        );

        return result;
    }

    public LiveData<ResponseWrapper<UserProfileUi>> getProfileUiLiveData() {
        return profileWrapperLiveData;
    }

}
