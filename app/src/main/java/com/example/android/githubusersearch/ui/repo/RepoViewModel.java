package com.example.android.githubusersearch.ui.repo;

import com.example.android.githubusersearch.data.ResponseWrapper;
import com.example.android.githubusersearch.data.remote.response.repo.Repo;
import com.example.android.githubusersearch.data.ui.RepoUi;
import com.example.android.githubusersearch.network.Api;
import com.example.android.githubusersearch.network.RetrofitClient;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoViewModel extends ViewModel {

    private Call<ArrayList<Repo>> call;
    private MutableLiveData<ResponseWrapper<ArrayList<RepoUi>>>
            repoLiveDataWrapper;

    public RepoViewModel(String username) {
        init(username);
    }

    void init(String username) {
        Api apiInstance = new RetrofitClient().getInstance();
        call = apiInstance.getReposList(username);

        repoLiveDataWrapper = new MutableLiveData<>();
    }

    void fetchRepoListFromNetwork() {
        final ResponseWrapper<ArrayList<RepoUi>> repoUiResponseWrapper
                = new ResponseWrapper<>();

        call.enqueue(new Callback<ArrayList<Repo>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Repo>> call,
                                   @NonNull Response<ArrayList<Repo>> response) {
                repoUiResponseWrapper.setSuccessful(response.isSuccessful());

                if (repoUiResponseWrapper.isNotSuccessful()){
                    repoUiResponseWrapper.setErrorMessage("Failure Retrieving data");
                    return;
                }

                ArrayList<Repo> responseBody = response.body();
                if (responseBody == null){
                    repoUiResponseWrapper.setErrorMessage("Null received from server");
                    return;
                }

                // Transforming from response to ui
                ArrayList<RepoUi> repoUiArrayList =
                        transformResponseToRepoUi(responseBody);

                // Setting error message and response body
                repoUiResponseWrapper.setErrorMessage("Successfully retrieved repo list");
                repoUiResponseWrapper.setResponseBody(repoUiArrayList);

                // Sending response wrapper through LiveData wrapper
                repoLiveDataWrapper.postValue(repoUiResponseWrapper);

            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Repo>> call,
                                  @NonNull Throwable t) {
                // Setting ResponseWrapper to unsuccessful and bad response message
                repoUiResponseWrapper.setSuccessful(false);
                repoUiResponseWrapper.setErrorMessage("Bad Response");

                repoLiveDataWrapper.postValue(repoUiResponseWrapper);
            }
        });
    }

    MutableLiveData<ResponseWrapper<ArrayList<RepoUi>>> getRepoLiveDataWrapper() {
        return repoLiveDataWrapper;
    }

    private ArrayList<RepoUi> transformResponseToRepoUi(ArrayList<Repo> repoArrayList){

        ArrayList<RepoUi> repoUiArrayList = new ArrayList<>();

        for (Repo currentRepo: repoArrayList
             ) {
            RepoUi currentRepoUi = new RepoUi();

            currentRepoUi.setRepoName(currentRepo.getName());
            currentRepoUi.setLanguage(currentRepo.getLanguage());

            repoUiArrayList.add(currentRepoUi);
        }
        return repoUiArrayList;
    }

}
