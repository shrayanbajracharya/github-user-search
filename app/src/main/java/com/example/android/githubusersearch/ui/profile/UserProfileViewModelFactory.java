package com.example.android.githubusersearch.ui.profile;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class UserProfileViewModelFactory implements ViewModelProvider.Factory {

    private String username;

    public UserProfileViewModelFactory(String username) {
        this.username = username;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new UserProfileViewModel(username);
    }
}
